# Description

A tool allowing to map MacOS keyboard shortcuts in Linux Gnome.

# Customize

Format of `keymap.json` is **Key to map from** > **Window class regexp** > **Key to map to**

Available modifiers:

- `<Alt>`
- `<Ctrl>`
- `<Shift>`
- `<Super>`
- `<NumLock>`
- `<CapsLock>`
- `<Mod1>`
- `<Mod2>`
- `<Mod3>`
- `<Mod4>`
- `<Mod5>`

Other keys names are are [xmodmap keysyms](http://wiki.linuxquestions.org/wiki/List_of_Keysyms_Recognised_by_Xmodmap).
You can find out a keysym for key by running `xev` in your terminal.

You can find out window class names by running:
```
LOGLEVEL=DEBUG ./grab.py
```


# Install

```
#!/bin/bash

mkdir -p ~/.local/bin ~/.local/share/applications ~/.config/grab ~/.config/autostart ~/.config/systemd/user

cp keymap.json ~/.config/grab/
cp grab.py ~/.local/bin/
sed s/USER/$USER/ toba/grab/grab.service > ~/.config/systemd/user/grab.service
cp grab.desktop ~/.local/share/applications/
cp grab.desktop ~/.config/autostart/
systemctl --user daemon-reload
systemctl --user start grab
```