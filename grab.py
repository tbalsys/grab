#!/usr/bin/python

import subprocess
import logging
import signal
import time
import json
import sys
import os
import re

from pathlib import Path
from Xlib.display import Display
from Xlib import X, XK, protocol, error

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
log = logging.getLogger('grab')

XDG_CACHE_HOME = os.environ.get('XDG_CACHE_HOME', os.path.expanduser('~/.cache'))
RUN_DIR = os.path.join(os.environ.get('XDG_RUNTIME_DIR', XDG_CACHE_HOME), "grab")
LOCK_FILE = os.path.join(RUN_DIR, "grab.pid")

class Grab:
	def __init__(self, disp, keys):
		if self.__verifyNotRunning():
			self.__createLockFile()

		self.keys = keys
		self.disp = disp
		self.root = self.disp.screen().root

	def __createLockFile(self):
		try:
			os.mkdir(RUN_DIR)
		except FileExistsError:
			pass

		with open(LOCK_FILE, "w") as lock_file:
			lock_file.write(str(os.getpid()))

	def __verifyNotRunning(self):
		if os.path.exists(LOCK_FILE):
			with open(LOCK_FILE, 'r') as lock_file:
				pid = lock_file.read()

			with subprocess.Popen(["ps", "-p", pid, "-o", "command"], stdout=subprocess.PIPE) as p:
				output = p.communicate()[0]

			if "grab" in output.decode():
				log.error(f"Already running as pid {pid}")
				sys.exit(1)

		return True

	@staticmethod
	def getWindowClass(window):
		wm_class = window.get_wm_class()
		if (wm_class and "FocusProxy" not in wm_class):
			return '.'.join(wm_class)
		else:
			parent = window.query_tree().parent
			if isinstance(parent, int):
				return ''
			else:
				return Grab.getWindowClass(parent)

	def key_press_event(self, window, key):
		return protocol.event.KeyPress(
			time = int(time.time()),
			root = self.root,
			window = window,
			same_screen = 0, child = X.NONE,
			root_x = 0, root_y = 0, event_x = 0, event_y = 0,
			state = key.modifiers,
			detail = key.keycode
			)

	def key_release_event(self, window, key):
		return protocol.event.KeyRelease(
			time = int(time.time()),
			root = self.root,
			window = window,
			same_screen = 0, child = X.NONE,
			root_x = 0, root_y = 0, event_x = 0, event_y = 0,
			state = key.modifiers,
			detail = key.keycode
			)

	def handle_event(self, event):
		if (event.__class__.__name__ not in ['KeyPress', 'KeyRelease']):
			log.debug(f"Unexpected event class {event.__class__.__name__}")
			return

		keycode = event.detail
		modifiers = event.state

		window = self.disp.get_input_focus().focus
		wm_class = Grab.getWindowClass(window)

		without_capslock_numlock = -(X.LockMask | X.Mod2Mask) - 1
		key_mapping = self.keys.get_key(keycode, modifiers & without_capslock_numlock, wm_class)
		if (key_mapping):
			self.disp.allow_events(X.AsyncKeyboard, X.CurrentTime)
			if event.type == X.KeyPress:
				log.debug(f'{key_mapping} KeyPress')
				event = self.key_press_event(window, key_mapping.key_to)
			elif event.type == X.KeyRelease:
				log.debug(f'{key_mapping} KeyRelease')
				event = self.key_release_event(window, key_mapping.key_to)
			window.send_event(event)
		else:
			self.disp.allow_events(X.ReplayKeyboard, event.time)

	def start(self):
		self.root.change_attributes(event_mask = X.KeyPressMask)

		for key_mapping in self.keys:
			log.info(f'Grabing key {key_mapping.key_from}')
			key_mapping.key_from.grab()

		signal.signal(signal.SIGINT, self.shutdown)
		signal.signal(signal.SIGTERM, self.shutdown)

		while True:
			self.handle_event(self.disp.next_event())

	def shutdown(self, signum, frame):
		for key_mapping in self.keys:
			log.info(f'Ungrabing key {key_mapping.key_from}')
			key_mapping.key_from.ungrab()
		try:
			os.remove(LOCK_FILE)
			log.info("Shutdown completed")
		except FileNotFoundError:
			log.error("Lock file was already removed")
			sys.exit(4)
		sys.exit(0)

class Key:
	MODIFIERS = {
		'<Alt>': X.Mod1Mask,
		'<Ctrl>': X.ControlMask,
		'<Shift>': X.ShiftMask,
		'<Super>': X.Mod4Mask,
		'<NumLock>': X.Mod2Mask,
		'<CapsLock>': X.LockMask,
		'<Mod1>': X.Mod1Mask,
		'<Mod2>': X.Mod2Mask,
		'<Mod3>': X.Mod3Mask,
		'<Mod4>': X.Mod4Mask,
		'<Mod5>': X.Mod5Mask,
	}

	__BAD_ACCESS = error.CatchError(error.BadAccess)

	def __init__(self, disp, key):
		self.key = key

		self.modifiers = 0
		for str_modifier in Key.MODIFIERS:
			if (key.find(str_modifier) != -1):
				key = key.replace(str_modifier, '')
				self.modifiers |= Key.MODIFIERS[str_modifier]

		keysym = XK.string_to_keysym(key)
		self.keycode = disp.keysym_to_keycode(keysym)
		if self.keycode == 0:
			raise ValueError(f'Bad key format: {self.key}')

		self.disp = disp

	def __str__(self):
		return f"{self.key} ({self.modifiers}, {self.keycode})"

	def __grab_key(self, addtional_modifiers = 0):
		self.disp.screen().root.grab_key(
			self.keycode,
			self.modifiers | addtional_modifiers,
			1,
			X.GrabModeSync,
			X.GrabModeSync,
			onerror=self.__BAD_ACCESS)
		self.disp.sync()
		if self.__BAD_ACCESS.get_error():
			log.error(f"Can't use {self.key}")

	def grab(self):
		self.__grab_key() # capslock off, numlock off
		self.__grab_key(X.LockMask) # capslock on, numlock off
		self.__grab_key(X.Mod2Mask) # capslock off, numlock on
		self.__grab_key(X.LockMask | X.Mod2Mask) # capslock on, numlock on

	def __ungrab_key(self, addtional_modifiers = 0):
		self.disp.screen().root.ungrab_key(self.keycode, self.modifiers | addtional_modifiers)

	def ungrab(self):
		self.__ungrab_key()
		self.__ungrab_key(X.LockMask)
		self.__ungrab_key(X.Mod2Mask)
		self.__ungrab_key(X.LockMask | X.Mod2Mask)

class Mapping:
	def __init__(self, key_from, regex, key_to):
		self.key_from = key_from
		self.regex = regex
		self.key_to = key_to

	def __str__(self):
		return f"{self.key_from} → {self.key_to}"

class KeyMap:
	def __init__(self, disp, keys):
		self.keycodes = {}
		for str_from, windows in keys.items():
			for window, str_to in windows.items():
				key_from = Key(disp, str_from)
				key_to = Key(disp, str_to)
				if (not key_from.keycode in self.keycodes):
					self.keycodes[key_from.keycode] = {}
				if (not key_from.modifiers in self.keycodes[key_from.keycode]):
					self.keycodes[key_from.keycode][key_from.modifiers] = {}
				if (not window in self.keycodes[key_from.keycode][key_from.modifiers]):
					self.keycodes[key_from.keycode][key_from.modifiers][window] = {}
				self.keycodes[key_from.keycode][key_from.modifiers][window] = Mapping(key_from, re.compile(window), key_to)

	def __iter__(self):
		for keycode in self.keycodes:
			for modifiers in self.keycodes[keycode]:
				for window in self.keycodes[keycode][modifiers]:
					yield self.keycodes[keycode][modifiers][window]

	def get_key(self, keycode, modifiers, wm_class):
		log.debug(f"Lookup {keycode} {modifiers} {wm_class}")
		if (keycode in self.keycodes and modifiers in self.keycodes[keycode]):
			for window in self.keycodes[keycode][modifiers]:
				if (self.keycodes[keycode][modifiers][window].regex.match(wm_class)):
					return self.keycodes[keycode][modifiers][window]
		
		return None

if __name__ == '__main__':
	disp = Display()

	try:
		jsonpath = f'{Path.home()}/.config/grab/keymap.json'
		with open(jsonpath, 'r') as read_file:
			data = json.load(read_file)
	except FileNotFoundError:
	 	log.error(f'File not found: {jsonpath}')
	 	sys.exit(2)

	try:
		keymap = KeyMap(disp, data)
	except ValueError as err:
		log.error(err)
		sys.exit(3)

	Grab(disp, keymap).start()
